/**
 * Next.js uses the App component to initialize pages. You can override it and control the page initialization. Which allows you to do amazing things like:

    Persisting layout between page changes
    Keeping state when navigating pages
    Custom error handling using componentDidCatch
    Inject additional data into pages (for example by processing GraphQL queries)
 */
import React from 'react';
import App, { Container } from 'next/app';
import { Provider } from 'react-redux';
import withRedux from "next-redux-wrapper";
import withReduxSaga from 'next-redux-saga';
import configureStore from '../core/store/configureStore';

class CRMApp extends App {
    static async getInitialProps ({ Component, ctx }) {
        let pageProps={};
        
        pageProps = CRMApp.loadComponentData(Component, ctx, pageProps);
        pageProps = CRMApp.loadGlobalData(ctx, pageProps);
    
        return {
          pageProps
        };
      }

      static loadGlobalData(ctx, pageProps) {
        return pageProps;
      }
    
      static async loadComponentData(Component, {store, isServer}, pageProps) {
        if (Component.getInitialProps) {
          pageProps = await Component.getInitialProps({store, isServer})
        }
        if (isServer && Component.getInitActions) {
          const actions = Component.getInitActions();
          actions.forEach(action => store.dispatch(action))
        }
        return pageProps;
      }
    
        
    render() {
        const { Component, pageProps, store } = this.props;

        return <Provider store={store}>
            <Container>
                <Component {...pageProps} />
            </Container>
        </Provider>
    }
}

export default withRedux(configureStore)(withReduxSaga(CRMApp));