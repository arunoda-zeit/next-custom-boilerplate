// _document is only rendered on the server side and not on the client side
// Event handlers like onClick can't be added to this file

// Import styled components ServerStyleSheet
import { ServerStyleSheet } from 'styled-components';

// ./pages/_document.js
import Document, { Head, Main, NextScript } from 'next/document';

class CRMDocument extends Document {
  static async getInitialProps(ctx: any) {

    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => sheet.collectStyles(<App {...props} />)
        })

      const initialProps = await Document.getInitialProps(ctx)
      return {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      }
    } finally {
      sheet.seal()
    }
  }

  render() {
    return <html>
        <Head>
          <style>{`body { margin: 0 }`}</style>
        </Head>
        <body className="custom_class">
          <Main />
          <NextScript />
        </body>
      </html>
  }
}

export default CRMDocument;