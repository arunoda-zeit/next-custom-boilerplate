import * as React from "react";
export interface HelloProps {
    compiler: String,
    framework: String
}

export const Home = (props: HelloProps) => <h1>Hello from {props.compiler} and {props.framework}!</h1>;

export default Home;