import React from 'react';
import {storiesOf} from '@storybook/react';
import Button from './Button.tsx';

storiesOf('Button', module).add('with text', () => {
    return <Button label="My Button" />
});