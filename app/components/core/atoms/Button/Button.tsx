import * as React from 'react';

type Props = {
    label: String
}

export default (props: Props) => <button>{props.label}</button>;