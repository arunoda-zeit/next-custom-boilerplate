import { all } from 'redux-saga/effects';
import { sagas } from '../../global';

export default function* () {
    yield all(sagas)
}