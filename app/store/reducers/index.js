import { combineReducers } from 'redux';
import { reducers } from '../../global';

export default combineReducers(reducers);