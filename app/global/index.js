import reducers from './reducers';
import sagas from './sagas';
import actions from './actions';

export { reducers };
export { sagas };
export { actions };

export default {
    reducers,
    sagas,
    actions
}