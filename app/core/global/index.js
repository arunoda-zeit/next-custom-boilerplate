import globalReducers from './reducers';
import globalSagas from './sagas';

export {
    globalReducers,
    globalSagas
}