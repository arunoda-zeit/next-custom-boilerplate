const withTypeScript = require('@zeit/next-typescript');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const getWebpackDevelopmentConfig = (config, { isServer }) => {
  if (isServer) {
    config.plugins.push(new ForkTsCheckerWebpackPlugin());
  };
  return config;
};
const getWebpackProductionConfig = (config, { isServer }) => config;

const webpackConfig = (config, { isServer, dev }) => {
  // Perform customizations to webpack config
  // Important: return the modified config
  if (dev) {
    config = getWebpackDevelopmentConfig(config, { isServer });
  }else {
    config = getWebpackProductionConfig(config, { isServer });
  }
  
  return config
};

module.exports = withTypeScript((phase, {defaultConfig}) => {
  return {
    webpack: webpackConfig,
    pageExtensions: ['jsx', 'js', 'ts']
  }
});