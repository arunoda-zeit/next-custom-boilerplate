# next-custom-boilerplate

This is a custom implementation of a boilerplate for a project based on React.

Features

    Next JS 8
    Typescript
    Hapi JS
    Storybook
    Jest
    Yeoman Generator

Getting Started
npm install

Build
npm run build

Development
npm run dev

Storybook
npm run storybook

Test
npm run test

Generator
