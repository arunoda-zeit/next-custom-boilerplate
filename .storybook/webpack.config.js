module.exports = {
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        'babel-preset-react-app'
                    ]
                }
            }
        ]
    }
}