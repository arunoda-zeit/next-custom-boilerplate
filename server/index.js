const Hapi = require('hapi');
const next = require('next');
const { server: config } = require('./config');
const initialize = require('./initialize');
const { 
    pathWrapper,
    defaultHandlerWrapper,
    nextHandlerWrapper
} = require('./next-wrapper');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });

const server = Hapi.server({
    host: config.host,
    port: config.port
});

server.route({
    method: 'GET',
    path: '/',
    handler: pathWrapper(app, '/')
});

server.route({
    method: 'GET',
    path: '/_next/{p*}' /* next specific routes */,
    handler: nextHandlerWrapper(app)
});

server.route({
    method: 'GET',
    path: '/{p*}' /* catch all route */,
    handler: defaultHandlerWrapper(app)
});

app.prepare().then(initialize(server));

